This module implements several Turing-type models,
most notably the one from Gierer and Meinhardt.

## Installation with Docker 

Create a new folder in your favorite location and change into it

```bash
mkdir DumuxTuring
cd DumuxTuring
```

Download the container startup script
```bash
wget https://git.iws.uni-stuttgart.de/dumux-appl/dumux-turing/-/raw/master/docker/pubtable_dumux-turing
```
Open the Docker Container
````bash
bash pubtable_dumux-turing open
````

## Installation from Source

Building from source requires a C++17 conforming compiler, CMake and pkg-config.
Clone this repository in your favourite location
```bash
git clone https://git.iws.uni-stuttgart.de/dumux-appl/dumux-turing.git
```

Download the dependencies
```bash
./dumux-turing/installdumux-turing.sh
```

Configure and build libraries
```bash
./dune-common/bin/dunecontrol --opts=./dumux/cmake.opts all
```

## Installation on Windows

See [this Wiki entry](https://git.iws.uni-stuttgart.de/dumux-appl/dumux-turing/-/wikis/Installation-on-Windows).

## Running Gierer-Meinhardt Simulations

The source code for the general Turing-type models is contained in
the subfolder `dumux/turing`. The specialization to Gierer-Meinhardt
and corresponding scenarios can be found in `test/gierermeinhardt`.

In order to run a simulation, change to the corresponding build folder and
execute the bash script `run_simulations.sh`:
```bash
cd ./dumux-turing/build-cmake/test/gierermeinhardt
./run_simulations.sh
```
This will build the executable and run the specified scenarios. By default,
this corresponds to the 1d setting with discontinuous diffusion coefficients
prescribed by the grid files `grids/DATA1D_ModPP_*.dgf`.
Adapt the script in order to run other scenarios.

## Dependencies on other DUNE modules

| module | branch |
|:-------|:-------|
| dune-\{core, uggrid\} | releases/2.9 |
| dumux | releases/3.8 |
