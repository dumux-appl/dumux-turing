// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TURING_VOLUME_VARIABLES_HH
#define DUMUX_TURING_VOLUME_VARIABLES_HH

namespace Dumux {

template<class Traits>
class TuringVolumeVariables
{
    using Scalar = typename Traits::PrimaryVariables::value_type;
    static constexpr int numEq = Traits::ModelTraits::numEq();

public:
    //! Export the type used for the primary variables
    using PrimaryVariables = typename Traits::PrimaryVariables;
    //! Export the type encapsulating primary variable indices
    using Indices = typename Traits::ModelTraits::Indices;

    /*!
     * \brief Updates all quantities for a given control volume.
     *
     * \param elemSol A vector containing all primary variables connected to the element
     * \param problem The object specifying the problem which ought to
     *                be simulated
     * \param element An element which contains part of the control volume
     * \param scv The sub-control volume
     */
    template<class ElemSol, class Problem, class Element, class Scv>
    void update(const ElemSol& elemSol,
                const Problem& problem,
                const Element& element,
                const Scv& scv)
    {
        priVars_ = elemSol[scv.localDofIndex()];
        for (int eqIdx = 0; eqIdx < numEq; ++eqIdx)
        {
            diffCoeffs_[eqIdx] = problem.diffCoeff(element, eqIdx);
        }
    }

    /*!
     * \brief Returns the vector of primary variables.
     */
    const PrimaryVariables &priVars() const
    { return priVars_; }

    /*!
     * \brief Returns a component of primary variable vector.
     *
     * \param pvIdx The index of the primary variable of interest
     */
    Scalar priVar(const int pvIdx) const
    { return priVars_[pvIdx]; }

    /*!
     * \brief Returns the diffusion coefficient for an equation.
     *
     * \param eqIdx The index of the PDE of interest
     */
    Scalar diffCoeff(const int eqIdx) const
    { return diffCoeffs_[eqIdx]; }

    /*!
     * \brief Returns how much the sub-control volume is extruded.
     *
     * This means the factor by which a lower-dimensional (1D or 2D)
     * entity needs to be expanded to get a full dimensional cell. The
     * default is 1.0 which means that 1D problems are actually
     * thought as pipes with a cross section of 1 m^2 and 2D problems
     * are assumed to extend 1 m to the back.
     */
    Scalar extrusionFactor() const
    { return 1.0; }

private:
    PrimaryVariables priVars_;
    std::array<Scalar, numEq> diffCoeffs_;
};

} // end namespace Dumux

#endif
