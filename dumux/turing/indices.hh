// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TURING_INDICES_HH
#define DUMUX_TURING_INDICES_HH

namespace Dumux {
// \{

template<int offset = 0>
struct TuringIndices
{
    static const int PVOffset = offset;      //!< the first index in primary variable vector
    static const int aIdx = PVOffset; //!< index of the component a
    static const int hIdx = PVOffset + 1; //!< index of the component h
    static const int uIdx = aIdx; //!< alternative notation for a
    static const int vIdx = hIdx; //!< alternative notation for h
};

// \}
} // end namespace

#endif
