// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_TURING_LOCAL_RESIDUAL_HH
#define DUMUX_TURING_LOCAL_RESIDUAL_HH

#include <dumux/common/properties.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/assembly/cclocalresidual.hh>
#include <dumux/discretization/cellcentered/tpfa/computetransmissibility.hh>
#include <dumux/discretization/extrusion.hh>

#include "indices.hh"

namespace Dumux {

template<class TypeTag>
class TuringLocalResidual : public GetPropType<TypeTag, Properties::BaseLocalResidual>
{
    using ParentType = GetPropType<TypeTag, Properties::BaseLocalResidual>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using Indices = TuringIndices<>;
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Extrusion = Extrusion_t<GridGeometry>;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    static constexpr int numComponents = ModelTraits::numComponents();
    static constexpr int dimWorld = GridView::dimensionworld;

public:
    using ParentType::ParentType;

    /*!
     * \brief Evaluate the rate of change of all conservation
     *        quantites (e.g. phase mass) within a sub-control volume.
     *
     * \param problem The problem
     * \param scv The sub control volume
     * \param volVars The current or previous volVars
     */
    NumEqVector computeStorage(const Problem& problem,
                               const SubControlVolume& scv,
                               const VolumeVariables& volVars) const
    {
        const static auto tau = getParam<Scalar>("Problem.Tau", 1.0);

        NumEqVector result(volVars.priVars());
        result[Indices::hIdx] *= tau;

        return result;
    }


    /*!
     * \brief Evaluate the flux over a face of a sub control volume.
     *
     * \param problem The problem
     * \param element The element
     * \param fvGeometry The finite volume geometry context
     * \param elemVolVars The volume variables for all flux stencil elements
     * \param scvf The sub control volume face to compute the flux on
     * \param elemFluxVarsCache The cache related to flux computation
     */
    NumEqVector computeFlux(const Problem& problem,
                            const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf,
                            const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
        NumEqVector flux;

        if constexpr (GetPropType<TypeTag, Properties::GridGeometry>::discMethod == DiscretizationMethods::cctpfa)
        {
            const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
            const auto& insideVolVars = elemVolVars[insideScv];
            const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

            for (int k = 0; k < numComponents; ++k)
            {
                const Scalar ti = computeTpfaTransmissibility(fvGeometry, scvf, insideScv, insideVolVars.diffCoeff(k),
                                                              insideVolVars.extrusionFactor());

                Scalar tij;

                if (scvf.boundary())
                    tij = scvf.area()*ti;
                else
                {
                    const auto outsideScvIdx = scvf.outsideScvIdx();
                    const auto& outsideScv = fvGeometry.scv(outsideScvIdx);

                    const Scalar tj = -computeTpfaTransmissibility(fvGeometry, scvf, outsideScv, outsideVolVars.diffCoeff(k),
                                                                   outsideVolVars.extrusionFactor());

                    tij = scvf.area()*(ti * tj)/(ti + tj);
                }

                const auto valInside = insideVolVars.priVar(k);
                const auto valOutside = outsideVolVars.priVar(k);

                flux[k] = tij*(valInside - valOutside);
            }
        }
        else
        {
            const auto& fluxVarCache = elemFluxVarsCache[scvf];

            for (int k = 0; k < numComponents; ++k)
            {
                const auto diffCoeff = elemVolVars[scvf.insideScvIdx()].diffCoeff(k);

                Dune::FieldVector<Scalar, dimWorld> gradU(0.0);
                for (auto&& scv : scvs(fvGeometry))
                {
                    const auto& volVars = elemVolVars[scv];

                    gradU.axpy(volVars.priVar(k), fluxVarCache.gradN(scv.indexInElement()));
                }

                flux[k] = -1.0*vtmv(scvf.unitOuterNormal(), diffCoeff, gradU)*Extrusion::area(scvf);
            }
        }

        return flux;
    }
};

} // end namespace Dumux

#endif
