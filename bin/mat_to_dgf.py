#!/usr/bin/python3

import scipy.io as sio
import sys, os
import argparse

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('mat_filename', help = "name of the MAT file")
    args = parser.parse_args()

    mat_filename = args.mat_filename
    mat_contents = sio.loadmat(mat_filename, squeeze_me=True)
    base_filename = os.path.splitext(mat_filename)[0]

    if "Data" in mat_contents: # 1d data
        for caseIdx in range(mat_contents['Data'].size):
            x = mat_contents['Data'][caseIdx][0]
            d = mat_contents['Data'][caseIdx][1]

            dgf_filename = base_filename + '_' + str(caseIdx) + '.dgf'

            with open(dgf_filename, 'w') as dgf_file:
                dgf_file.write("DGF\nvertex\n")

                for vertex in x:
                    dgf_file.write("{0}\n".format(vertex))

                dgf_file.write("#\nsimplex\nparameters 1\n")

                for vIdx in range(x.size - 1):
                    dgf_file.write("{0} {1} {2}\n".format(vIdx, vIdx+1, d[vIdx]))

                dgf_file.write("#\n# {0}\n".format(dgf_filename))

            print("Wrote DGF file", dgf_filename)
    else: # 2d data
        for caseIdx in range(mat_contents['Data_Points'].size):
            x = mat_contents['Data_Points'][caseIdx]
            t = mat_contents['Data_Simplex'][caseIdx]

            dgf_filename = base_filename + '_' + str(caseIdx) + '.dgf'

            with open(dgf_filename, 'w') as dgf_file:
                dgf_file.write("DGF\nvertex\n")

                for vertex in x:
                    dgf_file.write(f"{vertex[0]} {vertex[1]}\n")

                dgf_file.write("#\nsimplex\nparameters 1\n")

                for element in t:
                    dgf_file.write("{0} {1} {2} {3}\n".format(int(element[0]), int(element[1]), int(element[2]), element[3]))

                dgf_file.write("#\n# {0}\n".format(dgf_filename))

            print("Wrote DGF file", dgf_filename)

if __name__ == "__main__":
   main(sys.argv)

 
