# Convert 1d VTK output to NumPy arrays.
# On some Linux distributions such as Ubuntu 20.04,
# running this script as is might fail due to broken Python-Vtk,
# see https://bugs.archlinux.org/task/64891.
# This can be circumvented by preloading the netcdf library:
# LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libnetcdf.so.15 python3 read_vtp.py
# The correct library can be found by "locate libnetcdf".
import vtk
from vtk.util.numpy_support import vtk_to_numpy
import matplotlib.pyplot as plt
import numpy

reader = vtk.vtkXMLPolyDataReader()
reader.SetFileName('DATA1D_ModPP_1-00100.vtp')
reader.Update()
polyDataOutput = reader.GetOutput()

polydata = reader.GetOutput()
points = polydata.GetPoints()
array = points.GetData()
x = vtk_to_numpy(array)[:, 0]

a = vtk_to_numpy(polydata.GetCellData().GetArray('a'))
a = numpy.append(a, a[-1])

h = vtk_to_numpy(polydata.GetCellData().GetArray('h'))
h = numpy.append(h, h[-1])

D = vtk_to_numpy(polydata.GetCellData().GetArray('D'))
D = numpy.append(D, D[-1])

plt.step(x, a, where='post', label='a')
plt.step(x, h, where='post', label='h')
plt.legend()
plt.show()
