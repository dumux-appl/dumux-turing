'''Gierer-Meinhardt implementation in FEnics with backward-Euler scheme.
This module sets up the Gierer-Meinhardt system

        ∂a(t, ξ)/∂t = ε^2 * ∇·∇a(t, ξ) − a(t, ξ) + a(t, ξ)^2 / h(t, ξ)
    τ * ∂h(t, ξ)/∂t = ∇·(D(ξ) ∇h(t, ξ)) − h(t, ξ) + a(t, ξ)^2

in FEnics for

    the one-dimensional spatial domain ξ ∈ [mesh_left, mesh_right]
    for all times t ∈ [t_0, t_end]

with a backward-Euler integration scheme with constant time-step width.

The implementation follows along the lines of the offical demo for the Cahn-Hilliard equation
    https://fenicsproject.org/docs/dolfin/latest/python/demos/cahn-hilliard/demo_cahn-hilliard.py.html

Authors: SimTech PN5
Date:    Nov 2020
Contact: patrick.buchfink@ians.uni-stuttgart.de
'''
import os
import dolfin as df
import numpy as np

class GiererMeinhardt1DSolver(object):
    def __init__(self, n_spatial=500, epsilon=0.01, tau=0.1, diffusion=(8, 12), xb=-0.5, ictype=2, \
        mesh_left=-1, mesh_right=1, fpth_vis='paraview/gierermeinhardt_1d.pvd'):
        '''
        The parameters of the equation are:
            - n_spatial, the number of spatial discretization points
            - epsilon = ε, constant
            - tau = τ, constant
            - diffusion and xb, which specify the piece-wise constant diffusion coefficient D(ξ) with

                D(ξ) = diffusion[0] for mesh_left ≤ ξ ≤ xb
                D(ξ) = diffusion[1] for        xb ≤ ξ ≤ mesh_right

            - ictype, the type of the initial condition, which
            - mesh_left and mesh_right, which specify the spatial domain
            - fpth_vis, file path for visualization
        '''
        assert mesh_left <= xb <= mesh_right
        assert isinstance(diffusion, (tuple, list, np.ndarray)) and len(diffusion) == 2
        self.n_spatial = n_spatial
        self.epsilon = epsilon
        self.tau = tau
        self.diffusion = diffusion
        self.xb = xb
        self.ictype = ictype
        self.mesh_left = mesh_left
        self.mesh_right = mesh_right
        self.fpth_vis = fpth_vis

    def solve(self, t_0=0, t_end=1e2, dt=.1, \
        solver_parameters={'linear_solver': 'lu', 'convergence_criterion': 'incremental', 'relative_tolerance': 1e-6}):
        '''Solves the system with a backward-Euler integration scheme with constant time-step width
        and writes the solution to the path given in the constructor.
        Input:
            t_0, initial time
            t_end, final time
            dt, constant time-step width
            solver_parameters, parameters for FEnics Newton solver (default from Cahn-Hilliard official demo)
        '''

        # Create mesh and build function space
        mesh = df.IntervalMesh(self.n_spatial, self.mesh_left, self.mesh_right)
        P1 = df.FiniteElement("Lagrange", mesh.ufl_cell(), 1)
        ME = df.FunctionSpace(mesh, P1*P1)

        # devide domain in two subdomains each with constant diffusion coefficient
        domain_l = df.AutoSubDomain(lambda x, on_exterior: x[0] <= self.xb)
        domain_r = df.AutoSubDomain(lambda x, on_exterior: x[0] >= self.xb)
        mesh_fun = df.MeshFunction("size_t", mesh, mesh.topology().dim())
        domain_l.mark(mesh_fun, 0)
        domain_r.mark(mesh_fun, 1)
        subdomain_dx = df.Measure('dx', domain=mesh, subdomain_data=mesh_fun)
        
        # Define trial and test functions
        du = df.TrialFunction(ME)
        b, k = df.TestFunctions(ME)

        # Define functions
        u = df.Function(ME)  # current solution
        u0 = df.Function(ME)  # solution from previous converged step

        # Split mixed functions
        a, h = df.split(u)
        a0, h0 = df.split(u0)
        a_fun = u.split()[0]
        a_fun.rename(name='a', s='')
        h_fun = u.split()[1]
        h_fun.rename(name='h', s='')

        # Create intial conditions and interpolate
        u_init = InitialConditions(self.ictype, degree=1)
        u.interpolate(u_init)

        # visualize initial value
        self.init_visualize()
        self.visualize(a_fun, h_fun, t_0)

        # Weak statement of the equations
        # with backward-Euler time-discretization
        La = self.epsilon**2 * df.dot(df.grad(a), df.grad(b)) * df.dx \
            + ((a - a0)/dt + a - a**2/h) * b * df.dx
        Lh = (self.tau * (h - h0)/dt + h - a**2) * k * df.dx \
            + df.dot(df.grad(h), df.grad(k)) * self.diffusion[0] * subdomain_dx(0) \
            + df.dot(df.grad(h), df.grad(k)) * self.diffusion[1] * subdomain_dx(1)
        L = La + Lh

        # Compute directional derivative about u in the direction of du (Jacobian)
        dL_du = df.derivative(L, u, du)

        # Create nonlinear problem and Newton solver
        problem = GiererMeinhardtProblem(dL_du, L)
        solver = df.NewtonSolver()
        for key, value in solver_parameters.items():
            solver.parameters[key] = value

        # Step in time
        t = t_0
        while (t < t_end):
            t += dt
            u0.vector()[:] = u.vector()
            solver.solve(problem, u.vector())
            self.visualize(a_fun, h_fun, t)

    def init_visualize(self):
        # generate folders for visualization output
        folder, _ = os.path.split(self.fpth_vis)
        if not os.path.exists(folder):
            os.makedirs(folder)

        # create VTK file handles
        file_name, file_end = os.path.splitext(self.fpth_vis)
        self.vtk_file_a = df.File(file_name + '_a' + file_end, "compressed")
        self.vtk_file_h = df.File(file_name + '_h' + file_end, "compressed")

    def visualize(self, a_fun, h_fun, t):
        self.vtk_file_a << (a_fun, t)
        self.vtk_file_h << (h_fun, t)


class InitialConditions(df.UserExpression):
    '''A class representing the intial conditions.'''
    def __init__(self, ictype, **kwargs):
        super().__init__(**kwargs)
        self.ictype = ictype
        self.idx_a = 0
        self.idx_h = 1

    def eval(self, values, x):
        idx_a = self.idx_a
        idx_h = self.idx_h
        # set a
        if self.ictype == 1:
            values[idx_a] = 2 - np.sin(x/2.*np.pi)
        elif self.ictype == 2:
            values[idx_a] = 2 + np.sin(x/2.*np.pi)
        elif self.ictype == 3:
            values[idx_a] = 2 + np.cos(x*np.pi)
        elif self.ictype == 4:
            if x < 0.:
                values[idx_a] = 2 - np.cos(x*np.pi*2.)
            else:
                values[idx_a] = 1.
        elif self.ictype == 5:
            if x > 0.0:
                values[idx_a] = 2.0 - np.cos(x*np.pi*2.0)
            else:
                values[idx_a] = 1.0
        elif self.ictype == 6:
            if x < -0.5:
                values[idx_a] = 2.0 - np.cos(x*np.pi*4.0)
            else:
                values[idx_a] = 1.0
        elif self.ictype == 7:
            if x > -0.5 and x < 0.0:
                values[idx_a] = 2.0 - np.cos(x*np.pi*4.0)
            else:
                values[idx_a] = 1.0
        elif self.ictype == 8:
            if x > 0.0 and x < 0.5:
                values[idx_a] = 2.0 - np.cos(x*np.pi*4.0)
            else:
                values[idx_a] = 1.0
        elif self.ictype == 9:
            if x > 0.5:
                values[idx_a] = 2.0 - np.cos(x*np.pi*4.0)
            else:
                values[idx_a] = 1.0

        # set h
        values[idx_h] = 1.0

    def value_shape(self):
        return (2,)


class GiererMeinhardtProblem(df.NonlinearProblem):
    '''A class for interfacing with the Newton solver.'''
    def __init__(self, dL_du, L):
        df.NonlinearProblem.__init__(self)
        self.L = L
        self.dL_du = dL_du

    def F(self, b, x):
        df.assemble(self.L, tensor=b)

    def J(self, A, x):
        df.assemble(self.dL_du, tensor=A)


if __name__ == "__main__":
    gm_solver = GiererMeinhardt1DSolver()
    gm_solver.solve()
