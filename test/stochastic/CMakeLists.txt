add_input_file_links()
dune_symlink_to_source_files(FILES stochasticrhs.py)

find_package(Python REQUIRED)

execute_process(COMMAND python3-config --cflags OUTPUT_VARIABLE MY_PYTHON_CFLAGS)
separate_arguments(MY_PYTHON_CFLAGS UNIX_COMMAND "${MY_PYTHON_CFLAGS}")
add_compile_options(${MY_PYTHON_CFLAGS} -fPIE)

if(Python_VERSION GREATER_EQUAL 3.8)
    execute_process(COMMAND python3-config --embed --ldflags OUTPUT_VARIABLE MY_PYTHON_LDFLAGS)
else()
    execute_process(COMMAND python3-config --ldflags OUTPUT_VARIABLE MY_PYTHON_LDFLAGS)
endif()
separate_arguments(MY_PYTHON_LDFLAGS UNIX_COMMAND "${MY_PYTHON_LDFLAGS}")
link_libraries(${MY_PYTHON_LDFLAGS})

dumux_add_test(NAME test_stochastic
               SOURCES main.cc
               COMPILE_DEFINITIONS DIMWORLD=1 DISCRETIZATION=CCTpfaModel
               COMMAND ${dumux_INCLUDE_DIRS}/bin/testing/runtest.py
               CMD_ARGS --script fuzzy
                        --files ${CMAKE_SOURCE_DIR}/test/references/stochastic-reference.vtu
                                ${CMAKE_CURRENT_BINARY_DIR}/stochastic-00001.vtu
                        --command "${CMAKE_CURRENT_BINARY_DIR}/test_stochastic params.input -TimeLoop.TEnd 0.5")

# headers for installation and headercheck
install(FILES
        problem.hh
        stochasticrhs.py
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/test/stochastic)
