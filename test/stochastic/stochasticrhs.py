# ===================================================================================
# Stochastic Right Hand Side
# --------------------------
# Implementation for stochastic right hand sides for the Gierer-Meinhardt system.
# ===================================================================================

# import necessary packages
import numpy as np
import matplotlib.pyplot as plt

def createWienerNoise(finalTime, BrownianMotion=None, local_KL_truncation_BM=1000, KL_truncation_RF=10):
    """Compute a Wiener noise sample and return the corresponding lambda function."""

    # if no Brownian motion was given, compute one
    if BrownianMotion is None:
        BrownianMotion = createBrownianMotion(finalTime=finalTime, local_KL_truncation_BM=local_KL_truncation_BM, KL_truncation_RF=KL_truncation_RF)

    # compute eigenfunctions for spatial field expansion
    eigenFunctions_RF = createEigenfunction_spaceField()

    # construct Wiener noise
    WienerNoise = lambda t, x: (np.dot(BrownianMotion(np.array(t)).T, eigenFunctions_RF(np.arange(1, KL_truncation_RF+1)[np.newaxis], np.array(x)))).tolist()

    return WienerNoise


def createBrownianMotion(finalTime, KL_truncation_RF, local_KL_truncation_BM, scale_BM_truncation=False):
    """Compute a Brownian motion and return corresponding lambda function."""

    # computed scaled truncation for Brownian motion (corresponding to finalTime)
    if scale_BM_truncation:
        KL_truncation_BM = round(local_KL_truncation_BM * finalTime)
    else:
        KL_truncation_BM = local_KL_truncation_BM

    # compute standard-Normal-distributed random variables
    randomVariables = np.random.randn(KL_truncation_RF, KL_truncation_BM)

    # create eigenfunctions of Brownian motion
    eigenFunctions_BM = createEigenfunction_BrownianMotion(finalTime)

    # construct Brownian motion
    BrownianMotion = lambda t: np.dot(randomVariables, eigenFunctions_BM(np.arange(1, KL_truncation_BM+1)[np.newaxis], t))

    return BrownianMotion


def createEigenfunction_spaceField(gamma=2.5):
    """Compute eigenfunctions of -Laplace on (-1, 1).
    (cf. Hypothesis 2.1 in Hausenblas & Panda (2021))"""

    eigenfunction_RF = lambda eigIdx, x: np.multiply(
        np.sin( np.multiply(
            0.5 * np.pi * eigIdx.T, 
            1 + x
        )), 
        np.power(
            1 + 0.25 * np.pi**2 * (eigIdx.T)**2, 
            - gamma / 2
        )
    )

    return eigenfunction_RF


def createEigenfunction_BrownianMotion(finalTime):
    """This function defines the (scaled) eigenfunctions of the Brownian motion."""

    # define eigenfunction for given eigenfunction index as time-dependent function.
    eigenfunction_BM = lambda eigIdx, t: np.multiply(
        np.divide(
            2 * np.sqrt(2 * finalTime), 
            np.multiply(2 * eigIdx.T - 1, np.pi)
        ), 
        np.sin(
            np.divide(
                np.multiply((2 * eigIdx.T - 1), np.pi * t),
                2 * finalTime
            )
        )
    )

    return eigenfunction_BM


def visualizeWienerNoise(X_grid, Y_grid, noise):
    """This function visualizes the provided Wiener noise."""

    # create figure and plot noise
    cFig, cAxes = plt.subplots(subplot_kw={"projection": "3d"})
    cAxes.plot_surface(X_grid, Y_grid, noise)

    #plt.savefig('test.png')


if __name__ == '__main__':
    # create space-time discretization
    finalTime      = 1
    timeInterval   = [0, finalTime]
    spaceDomain    = [-1, 1]
    spacePoints    = np.linspace(spaceDomain[0], spaceDomain[1], 500)[np.newaxis]
    timePoints     = np.linspace(timeInterval[0], timeInterval[1], 1000)[np.newaxis]
    X_grid, T_grid = np.meshgrid(timePoints, spacePoints)

    # create Wiener noise
    myNoiseHandle = createWienerNoise(finalTime)
    myNoise       = np.array(myNoiseHandle(timePoints, spacePoints))
    visualizeWienerNoise(T_grid, X_grid, myNoise.T)
