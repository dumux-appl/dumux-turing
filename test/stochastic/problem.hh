// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GIERERMEINHARDT_PROBLEM_HH
#define DUMUX_GIERERMEINHARDT_PROBLEM_HH

#include <fstream>

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/fvproblem.hh>
#include <dumux/common/timeloop.hh>
#include <dumux/io/grid/griddata.hh>

#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace Dumux {

template <class TypeTag >
class GiererMeinhardtProblem : public FVProblem<TypeTag>
{
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;

    static constexpr int aIdx = Indices::aIdx;
    static constexpr int hIdx = Indices::hIdx;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    // obtain diffusion coefficients from parameter input file
    GiererMeinhardtProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), diffCoeffs_(fvGridGeometry->numScv())
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto eIdx = this->gridGeometry().elementMapper().index(element);

            if (hasParam("Problem.Xb")) // discontinuity points
            {
                const static auto xbs = getParam<std::vector<Scalar>>("Problem.Xb");
                const static auto diffCoeffs = getParam<std::vector<Scalar>>("Problem.D");
                assert(xbs.size() == diffCoeffs.size() - 1);

                // the following assumes an ascending order of xbs
                diffCoeffs_[eIdx] = diffCoeffs[xbs.size()];
                for (auto i = 0u; i < xbs.size(); ++i)
                {
                    if (element.geometry().center()[0] < xbs[i])
                    {
                        diffCoeffs_[eIdx] = diffCoeffs[i];
                        break;
                    }
                }
            }
            else // uniform diffusion coefficient
            {
                const static auto diffCoeff = getParam<Scalar>("Problem.D");
                diffCoeffs_[eIdx] = diffCoeff;
            }
        }

        createWienerNoise_();
        wiener_.resize(fvGridGeometry->numScv());
        oldWiener_.resize(fvGridGeometry->numScv());
    }

    // obtain diffusion coefficients from DGF file
    GiererMeinhardtProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                           std::shared_ptr<const GridData<Grid>> gridData)
    : ParentType(fvGridGeometry), diffCoeffs_(fvGridGeometry->numScv())
    {
        // Dune's DGF parameter functionality only works for 2d and 3d
        if (GridView::dimensionworld > 1)
        {
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                const auto eIdx = this->gridGeometry().elementMapper().index(element);
                diffCoeffs_[eIdx] = gridData->parameters(element)[0];
            }
        }
        else // manually extract the parameters from the DGF file
        {
            std::string str;
            std::ifstream dgfFile(getParam<std::string>("Grid.File"));

            while (dgfFile >> str)
            {
                if (str == "parameters")
                {
                    int dummy1, dummy2;
                    dgfFile >> dummy1;

                    for (const auto& element : elements(this->gridGeometry().gridView()))
                    {
                        const auto eIdx = this->gridGeometry().elementMapper().index(element);
                        dgfFile >> dummy1 >> dummy2 >> diffCoeffs_[eIdx];
                    }

                    break;
                }
            }
            dgfFile.close();
        }

        createWienerNoise_();
        wiener_.resize(fvGridGeometry->numScv());
        oldWiener_.resize(fvGridGeometry->numScv());
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        bcTypes.setAllNeumann();

        return bcTypes;
    }

    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source;

        const auto& priVars = elemVolVars[scv].priVars();

        source[aIdx] = priVars[aIdx]*priVars[aIdx]/priVars[hIdx] - priVars[aIdx];
        source[hIdx] = priVars[aIdx]*priVars[aIdx] - priVars[hIdx];

        const auto oldWiener = oldWiener_[scv.dofIndex()];
        const auto wiener = wiener_[scv.dofIndex()];

        const static auto sigma = getParam<NumEqVector>("Problem.Sigma");
        const auto& oldPriVars = (*oldSol_)[scv.dofIndex()];
        const auto dt = timeLoop_->timeStepSize();

        source[aIdx] += sigma[aIdx]*0.5*(priVars[aIdx] + oldPriVars[aIdx])*(wiener[aIdx] - oldWiener[aIdx])/dt;
        source[hIdx] += sigma[hIdx]*0.5*(priVars[hIdx] + oldPriVars[hIdx])*(wiener[hIdx] - oldWiener[hIdx])/dt;

        return source;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        const static auto icType = getParam<int>("Problem.ICType", 1);

        PrimaryVariables values;
        values[hIdx] = 1.0;

        Scalar product = 1.0;
        for (int coordIdx = 0; coordIdx < GridView::dimensionworld; ++coordIdx)
            product *= sin(globalPos[coordIdx]*M_PI/2.0);

        Scalar coshx;
        switch (icType)
        {
            case 1:
                values[aIdx] = 2.0 - product;
                break;
            case 2:
                values[aIdx] = 2.0 + product;
                break;
            case 3:
                values[aIdx] = 2.0 + cos(globalPos[0]*M_PI);
                break;
            case 4:
                if (globalPos[0] < 0.0)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*2.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 5:
                if (globalPos[0] > 0.0)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*2.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 6:
                if (globalPos[0] < -0.5)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 7:
                if (globalPos[0] > -0.5 && globalPos[0] < 0.0)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 8:
                if (globalPos[0] > 0.0 && globalPos[0] < 0.5)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 9:
                if (globalPos[0] > 0.5)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 10:
                const static auto a = getParam<Scalar>("Problem.ICParamA", 10.0);
                const static auto b = getParam<Scalar>("Problem.ICParamB", 0.0);
                const static auto c = getParam<Scalar>("Problem.ICParamC", 0.05);
                coshx = std::cosh((globalPos[0] - b)/(2.0*c));
                values[aIdx] = 1.5*a/(coshx*coshx);
                break;
            default:
                DUNE_THROW(Dune::NotImplemented, "IC Type " + std::to_string(icType));
        }

        return values;
    }

    Scalar diffCoeff(const Element &element, int eqIdx) const
    {
        const static auto epsilon = getParam<Scalar>("Problem.Epsilon");

        if (eqIdx == aIdx)
            return epsilon*epsilon;
        else
            return diffCoeffs_[this->gridGeometry().elementMapper().index(element)];
    }

    void setTimeLoop(std::shared_ptr<TimeLoop<Scalar>> timeLoop)
    {
        timeLoop_ = timeLoop;
    }

    void setOldSolution(SolutionVector *oldSol)
    {
        oldSol_ = oldSol;
    }

    void updateWienerNoise()
    {
        // This tuple is passed to the Python function.
        // Its first component is a list of time values,
        // its second a list of space values.
        auto pythonArgs = PyTuple_New(2);

        const auto t = timeLoop_->time();
        const auto dt = timeLoop_->timeStepSize();
        // evaluate at old and new time
        auto pythonTime = PyList_New(2);
        PyList_SetItem(pythonTime, 0, PyFloat_FromDouble(t));
        PyList_SetItem(pythonTime, 1, PyFloat_FromDouble(t + dt));
        PyTuple_SetItem(pythonArgs, 0, pythonTime);

        // assumes a cell-centered discretization and that the domain is 1D
        const auto size = wiener_.size();
        auto pythonSpace = PyList_New(size);
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto eIdx = this->gridGeometry().elementMapper().index(element);
            PyList_SetItem(pythonSpace, eIdx, PyFloat_FromDouble(element.geometry().center()[0]));
        }
        PyTuple_SetItem(pythonArgs, 1, pythonSpace);

        // The result of each Python call is a list of 2 lists,
        // where the first contains the Wiener noise at the old time step for each space point
        // and the second the one at the new time step.
        auto pythonValuesA = PyObject_CallObject(pythonWienerNoiseA_, pythonArgs);
        auto pythonValuesH = PyObject_CallObject(pythonWienerNoiseH_, pythonArgs);

        // Get the Wiener noise at the individual time steps
        auto pythonOldWienerA = PyList_GetItem(pythonValuesA, 0);
        auto pythonWienerA = PyList_GetItem(pythonValuesA, 1);
        auto pythonOldWienerH = PyList_GetItem(pythonValuesH, 0);
        auto pythonWienerH = PyList_GetItem(pythonValuesH, 1);
        for (std::size_t i = 0; i != size; ++i)
        {
            oldWiener_[i][aIdx] = PyFloat_AsDouble(PyList_GetItem(pythonOldWienerA, i));
            oldWiener_[i][hIdx] = PyFloat_AsDouble(PyList_GetItem(pythonOldWienerH, i));
            wiener_[i][aIdx] = PyFloat_AsDouble(PyList_GetItem(pythonWienerA, i));
            wiener_[i][hIdx] = PyFloat_AsDouble(PyList_GetItem(pythonWienerH, i));
        }

        Py_DECREF(pythonWienerH);
        Py_DECREF(pythonOldWienerH);
        Py_DECREF(pythonWienerA);
        Py_DECREF(pythonOldWienerA);
        Py_DECREF(pythonValuesH);
        Py_DECREF(pythonValuesA);
        Py_DECREF(pythonSpace);
        Py_DECREF(pythonTime);
        Py_DECREF(pythonArgs);
    }

    ~GiererMeinhardtProblem()
    {
        Py_DECREF(pythonWienerNoiseH_);
        Py_DECREF(pythonWienerNoiseA_);
        Py_Finalize();
    }

private:
    void createWienerNoise_()
    {
        Py_Initialize();

        const static auto moduleName = getParam<std::string>("Problem.PythonModule", "stochasticrhs");
        auto pythonModule = PyImport_ImportModule(moduleName.c_str());

        const static auto createFuncName = getParam<std::string>("Problem.CreateFuncName", "createWienerNoise");
        auto createFunc = PyObject_GetAttrString(pythonModule, createFuncName.c_str());

        const static auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
        auto pythonValue = PyFloat_FromDouble(tEnd);
        auto pythonArgs = PyTuple_New(1);
        PyTuple_SetItem(pythonArgs, 0, pythonValue);
        pythonWienerNoiseA_ = PyObject_CallObject(createFunc, pythonArgs);
        pythonWienerNoiseH_ = PyObject_CallObject(createFunc, pythonArgs);

        Py_DECREF(pythonArgs);
        Py_DECREF(pythonValue);
        Py_DECREF(createFunc);
        Py_DECREF(pythonModule);
    }

    std::vector<Scalar> diffCoeffs_;
    PyObject *pythonWienerNoiseA_;
    PyObject *pythonWienerNoiseH_;
    std::shared_ptr<TimeLoop<Scalar>> timeLoop_;
    SolutionVector *oldSol_;
    std::vector<NumEqVector> wiener_;
    std::vector<NumEqVector> oldWiener_;
};

} //end namespace Dumux

#endif // DUMUX_GIERERMEINHARDT_PROBLEM_HH
