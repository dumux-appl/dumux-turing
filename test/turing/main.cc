// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#include "config.h"
#include "problem.hh"

#include <ctime>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>
#include <dune/grid/io/file/dgfparser/dgfexception.hh>
#include <dune/grid/io/file/vtk.hh>
#include <dune/istl/io.hh>
#include <dune/grid/yaspgrid.hh>

#include <dumux/common/properties.hh>
#include <dumux/common/parameters.hh>
#include <dumux/common/dumuxmessage.hh>
#include <dumux/common/defaultusagemessage.hh>

#include <dumux/linear/linearalgebratraits.hh>
#include <dumux/linear/linearsolvertraits.hh>
#include <dumux/linear/istlsolvers.hh>
#include <dumux/linear/pdesolver.hh>
#include <dumux/linear/seqsolverbackend.hh>
#include <dumux/nonlinear/newtonsolver.hh>

#include <dumux/assembly/fvassembler.hh>
#include <dumux/assembly/diffmethod.hh>

#include <dumux/discretization/method.hh>
#include <dumux/io/grid/gridmanager.hh>
#include <dumux/io/vtkoutputmodule.hh>

#include <dumux/discretization/cctpfa.hh>
#include <dumux/turing/model.hh>

namespace Dumux {
namespace Properties {

namespace TTag {
struct TuringTpfa { using InheritsFrom = std::tuple<Turing, CCTpfaModel>; };
}

template<class TypeTag>
struct Grid<TypeTag, TTag::TuringTpfa> { using type = Dune::YaspGrid<2>; };

template<class TypeTag>
struct Problem<TypeTag, TTag::TuringTpfa> { using type = TuringProblem<TypeTag>; };

template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TuringTpfa> { static constexpr bool value = true; };

template<class TypeTag>
struct EnableGridGeometryCache<TypeTag, TTag::TuringTpfa> { static constexpr bool value = true; };

} // end namespace Properties
} // end namespace Dumux

void usage(const char *progName, const std::string &errorMsg)
{
    if (errorMsg.size() > 0) {
        std::string errorMessageOut = "\nUsage: ";
                    errorMessageOut += progName;
                    errorMessageOut += " [options]\n";
                    errorMessageOut += errorMsg;
                    errorMessageOut += "\n\nThe list of important arguments for this program is:\n"
                            "\t-TimeLoop.TEnd             end time of the simulation\n"
                            "\t-TimeLoop.DtInitial        initial time step size\n"
                            "\t-TimeLoop.MaxTimeStepSize  maximal time step size\n"
                            "\t-Grid.LowerLeft            lower left (front) corner of the domain\n"
                            "\t-Grid.UpperRight           upper right (back) corner of the domain\n"
                            "\t-Grid.Cells                grid resolution in each coordinate direction\n"
                            "\t-Problem.A, .B, .Gamma     model constants\n"
                            "\t-Problem.DiffCoeff         diffusion coefficients\n"
                            "\t-Problem.OutputInterval    interval size for VTK output\n"
                            "\t-Problem.Name              base name for VTK output files\n";
        std::cout << errorMessageOut
                  << "\n";
    }
}

int main(int argc, char** argv) try
{
    using namespace Dumux;

    // define the type tag for this problem
    using TypeTag = Properties::TTag::TuringTpfa;

    // initialize MPI, finalize is done automatically on exit
    const auto& mpiHelper = Dune::MPIHelper::instance(argc, argv);

    // print dumux start message
    if (mpiHelper.rank() == 0)
        DumuxMessage::print(/*firstCall=*/true);

    // parse command line arguments and input file
    Parameters::init(argc, argv);

    // try to create a grid (from the given grid file or the input file)
    GridManager<GetPropType<TypeTag, Properties::Grid>> gridManager;
    gridManager.init();

    ////////////////////////////////////////////////////////////
    // run instationary non-linear problem on this grid
    ////////////////////////////////////////////////////////////

    // we compute on the leaf grid view
    const auto& leafGridView = gridManager.grid().leafGridView();

    // create the finite volume grid geometry
    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    auto gridGeometry = std::make_shared<GridGeometry>(leafGridView);

    // the problem (initial and boundary conditions)
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    auto problem = std::make_shared<Problem>(gridGeometry);

    // get some time loop parameters
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    const auto tEnd = getParam<Scalar>("TimeLoop.TEnd");
    const auto maxDt = getParam<Scalar>("TimeLoop.MaxTimeStepSize");
    auto dt = getParam<Scalar>("TimeLoop.DtInitial");

    // the solution vector
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    SolutionVector x; problem->applyInitialSolution(x);
    auto xOld = x;

    // the grid variables
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    auto gridVariables = std::make_shared<GridVariables>(problem, gridGeometry);
    gridVariables->init(x);

    // intialize the vtk output module
    using IOFields = GetPropType<TypeTag, Properties::IOFields>;
    VtkOutputModule<GridVariables, SolutionVector> vtkWriter(*gridVariables, x, problem->name());
    IOFields::initOutputModule(vtkWriter); //!< Add model specific output fields
    vtkWriter.write(0.0);

    // instantiate time loop
    auto timeLoop = std::make_shared<CheckPointTimeLoop<Scalar>>(0.0, dt, tEnd);
    timeLoop->setMaxTimeStepSize(maxDt);

    bool enableCheckPoints = hasParam("Problem.OutputInterval");
    if (enableCheckPoints)
        timeLoop->setPeriodicCheckPoint(getParam<double>("Problem.OutputInterval"));

    // the assembler with time loop for instationary problem
    using ExplicitAssembler = FVAssembler<TypeTag, DiffMethod::numeric, /*implicit=*/false>;
    auto explicitAssembler = std::make_shared<ExplicitAssembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using ExplicitLinearSolver = ExplicitDiagonalSolver;
    auto explicitLinearSolver = std::make_shared<ExplicitLinearSolver>();

    // the non-linear solver
    using PDESolver = Dumux::LinearPDESolver<ExplicitAssembler, ExplicitLinearSolver>;
    PDESolver explicitNonLinearSolver(explicitAssembler, explicitLinearSolver);

    // the assembler with time loop for instationary problem
    using ImplicitAssembler = FVAssembler<TypeTag, DiffMethod::numeric>;
    auto implicitAssembler = std::make_shared<ImplicitAssembler>(problem, gridGeometry, gridVariables, timeLoop, xOld);

    // the linear solver
    using ImplicitLinearSolver = Dumux::UMFPackIstlSolver<SeqLinearSolverTraits, LinearAlgebraTraitsFromAssembler<ImplicitAssembler>>;
    auto implicitLinearSolver = std::make_shared<ImplicitLinearSolver>();

    // the non-linear solver
    using NewtonSolver = Dumux::NewtonSolver<ImplicitAssembler, ImplicitLinearSolver>;
    NewtonSolver implicitNonLinearSolver(implicitAssembler, implicitLinearSolver);

    auto theta = getParam<double>("TimeLoop.Theta", 1.0);

    // time loop
    timeLoop->start(); do
    {
        // linearize & solve
        if (theta < 1.0 - 1e-6)
            explicitNonLinearSolver.solve(x);

        auto explicitX = x;

        if (theta > 1e-6)
            implicitNonLinearSolver.solve(x, *timeLoop);

        x *= theta;
        explicitX *= 1.0 - theta;
        x += explicitX;

        if (std::isnan(x[0][0]))
        {
            std::cerr << "\nSolution vector contains nan. Abort.\n";
            exit(1);
        }

        // make the new solution the old solution
        xOld = x;
        gridVariables->advanceTimeStep();

        // advance to the time loop to the next step
        timeLoop->advanceTimeStep();

        // report statistics of this time step
        timeLoop->reportTimeStep();

        // set new dt as suggested by the newton solver
        if (theta > 1e-6)
            timeLoop->setTimeStepSize(implicitNonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));
        else
            timeLoop->setTimeStepSize(explicitNonLinearSolver.suggestTimeStepSize(timeLoop->timeStepSize()));

        if (timeLoop->isCheckPoint() || timeLoop->finished() || !enableCheckPoints)
            vtkWriter.write(timeLoop->time());

    } while (!timeLoop->finished());

    timeLoop->finalize(leafGridView.comm());

    ////////////////////////////////////////////////////////////
    // finalize, print dumux message to say goodbye
    ////////////////////////////////////////////////////////////

    // print dumux end message
    if (mpiHelper.rank() == 0)
    {
        Parameters::print();
        DumuxMessage::print(/*firstCall=*/false);
    }

    return 0;
}
catch (Dumux::ParameterException &e)
{
    std::cerr << std::endl << e << " ---> Abort!" << std::endl;
    return 1;
}
catch (Dune::Exception &e)
{
    std::cerr << "Dune reported error: " << e << " ---> Abort!" << std::endl;
    return 3;
}
catch (...)
{
    std::cerr << "Unknown exception thrown! ---> Abort!" << std::endl;
    return 4;
}
