// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_GIERERMEINHARDT_PROBLEM_HH
#define DUMUX_GIERERMEINHARDT_PROBLEM_HH

#include <fstream>

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/fvproblem.hh>
#include <dumux/common/numeqvector.hh>
#include <dumux/io/grid/griddata.hh>

namespace Dumux {

template <class TypeTag >
class GiererMeinhardtProblem : public FVProblem<TypeTag>
{
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Grid = GetPropType<TypeTag, Properties::Grid>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr int aIdx = Indices::aIdx;
    static constexpr int hIdx = Indices::hIdx;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    // obtain diffusion coefficients from parameter input file
    GiererMeinhardtProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry), diffCoeffs_(fvGridGeometry->numScv())
    {
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            const auto eIdx = this->gridGeometry().elementMapper().index(element);

            if (hasParam("Problem.Xb")) // discontinuity points
            {
                const static auto xbs = getParam<std::vector<Scalar>>("Problem.Xb");
                const static auto diffCoeffs = getParam<std::vector<Scalar>>("Problem.D");
                assert(xbs.size() == diffCoeffs.size() - 1);

                // the following assumes an ascending order of xbs
                diffCoeffs_[eIdx] = diffCoeffs[xbs.size()];
                for (auto i = 0u; i < xbs.size(); ++i)
                {
                    if (element.geometry().center()[0] < xbs[i])
                    {
                        diffCoeffs_[eIdx] = diffCoeffs[i];
                        break;
                    }
                }
            }
            else // uniform diffusion coefficient
            {
                const static auto diffCoeff = getParam<Scalar>("Problem.D", 1.0);
                diffCoeffs_[eIdx] = diffCoeff;
            }
        }
    }

    // obtain diffusion coefficients from DGF file
    GiererMeinhardtProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry,
                           std::shared_ptr<const GridData<Grid>> gridData)
    : ParentType(fvGridGeometry), diffCoeffs_(fvGridGeometry->numScv())
    {
        // Dune's DGF parameter functionality only works for 2d and 3d
        if (GridView::dimensionworld > 1)
        {
            for (const auto& element : elements(this->gridGeometry().gridView()))
            {
                const auto eIdx = this->gridGeometry().elementMapper().index(element);
                diffCoeffs_[eIdx] = gridData->parameters(element)[0];
            }
        }
        else // manually extract the parameters from the DGF file
        {
            std::string str;
            std::ifstream dgfFile(getParam<std::string>("Grid.File"));

            while (dgfFile >> str)
            {
                if (str == "parameters")
                {
                    int dummy1, dummy2;
                    dgfFile >> dummy1;

                    for (const auto& element : elements(this->gridGeometry().gridView()))
                    {
                        const auto eIdx = this->gridGeometry().elementMapper().index(element);
                        dgfFile >> dummy1 >> dummy2 >> diffCoeffs_[eIdx];
                    }

                    break;
                }
            }
            dgfFile.close();
        }
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        bcTypes.setAllNeumann();

        return bcTypes;
    }

    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source;

        const auto& priVars = elemVolVars[scv].priVars();

        source[aIdx] = priVars[aIdx]*priVars[aIdx]/priVars[hIdx] - priVars[aIdx];
        source[hIdx] = priVars[aIdx]*priVars[aIdx] - priVars[hIdx];

        return source;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        const static auto icType = getParam<int>("Problem.ICType", 1);

        PrimaryVariables values;
        values[hIdx] = 1.0;

        Scalar product = 1.0;
        for (int coordIdx = 0; coordIdx < GridView::dimensionworld; ++coordIdx)
            product *= sin(globalPos[coordIdx]*M_PI/2.0);

        Scalar coshx;
        switch (icType)
        {
            case 1:
                values[aIdx] = 2.0 - product;
                break;
            case 2:
                values[aIdx] = 2.0 + product;
                break;
            case 3:
                values[aIdx] = 2.0 + cos(globalPos[0]*M_PI);
                break;
            case 4:
                if (globalPos[0] < 0.0)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*2.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 5:
                if (globalPos[0] > 0.0)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*2.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 6:
                if (globalPos[0] < -0.5)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 7:
                if (globalPos[0] > -0.5 && globalPos[0] < 0.0)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 8:
                if (globalPos[0] > 0.0 && globalPos[0] < 0.5)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 9:
                if (globalPos[0] > 0.5)
                    values[aIdx] = 2.0 - cos(globalPos[0]*M_PI*4.0);
                else
                    values[aIdx] = 1.0;
                break;
            case 10:
                const static auto a = getParam<Scalar>("Problem.ICParamA", 10.0);
                const static auto b = getParam<Scalar>("Problem.ICParamB", 0.0);
                const static auto c = getParam<Scalar>("Problem.ICParamC", 0.05);
                coshx = std::cosh((globalPos[0] - b)/(2.0*c));
                values[aIdx] = 1.5*a/(coshx*coshx);
                break;
            default:
                DUNE_THROW(Dune::NotImplemented, "IC Type " + std::to_string(icType));
        }

        return values;
    }

    Scalar diffCoeff(const Element &element, int eqIdx) const
    {
        const static auto epsilon = getParam<Scalar>("Problem.Epsilon");

        if (eqIdx == aIdx)
            return epsilon*epsilon;
        else
            return diffCoeffs_[this->gridGeometry().elementMapper().index(element)];
    }

private:
    std::vector<Scalar> diffCoeffs_;
};

} //end namespace Dumux

#endif // DUMUX_GIERERMEINHARDT_PROBLEM_HH
