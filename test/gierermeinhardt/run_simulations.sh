#!/bin/bash

DIMENSION=1
NUM_SIMS=10
BASENAME="DATA1D_ModPP"

if [ "$(basename $PWD)" != "gierermeinhardt" ]; then
    echo "You have to run the script from the gierermeinhardt folder!"
    exit -1
fi

make test_gierermeinhardt_${DIMENSION}d
for i in $(seq 0 $((NUM_SIMS - 1))); do
    PROBLEMNAME=$(echo "${BASENAME}_${i}");
    DGFFILENAME=$(echo "${BASENAME}_${i}.dgf");
    ./test_gierermeinhardt_${DIMENSION}d params_${DIMENSION}d.input -Grid.File grids/$DGFFILENAME -Problem.Name $PROBLEMNAME
done

if [ -x "$(command -v paraview)" ]; then
    paraview ${BASENAME}*pvd &
else
    if [ -d "/dumux/shared" ]; then
        cp ${BASENAME}*pvd ${BASENAME}*vtu ${BASENAME}*vtp /dumux/shared/.
        echo "Copied the result files to the folder /dumux/shared which corresponds to the"
        echo "folder on the host from where the Docker container has been started."
        echo "Go to this folder in the host system and execute \"paraview ${BASENAME}*pvd\"."
    else
        echo "Paraview could not be found on your system. Try to execute it manually and"
        echo "pass the pvd files ${BASENAME}*pvd."
    fi
fi
