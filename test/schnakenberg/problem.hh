// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_SCHNAKENBERG_PROBLEM_HH
#define DUMUX_SCHNAKENBERG_PROBLEM_HH

#include <dumux/common/boundarytypes.hh>
#include <dumux/common/fvproblem.hh>
#include <dumux/common/numeqvector.hh>

namespace Dumux {

template <class TypeTag >
class SchnakenbergProblem : public FVProblem<TypeTag>
{
    using ParentType = FVProblem<TypeTag>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using FVGridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using BoundaryTypes = Dumux::BoundaryTypes<GetPropType<TypeTag, Properties::ModelTraits>::numEq()>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = Dumux::NumEqVector<PrimaryVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;

    static constexpr int uIdx = Indices::uIdx;
    static constexpr int vIdx = Indices::vIdx;

    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;

public:
    SchnakenbergProblem( std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
        a_ = getParam<Scalar>("Problem.A");
        b_ = getParam<Scalar>("Problem.B");
        gamma_ = getParam<Scalar>("Problem.Gamma");
    }

    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes bcTypes;

        bcTypes.setAllNeumann();

        return bcTypes;
    }

    NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
    {
        NumEqVector source;

        const auto& priVars = elemVolVars[scv].priVars();

        source[uIdx] = gamma_*(a_ - priVars[uIdx] + priVars[uIdx]*priVars[uIdx]*priVars[vIdx]);
        source[vIdx] = gamma_*(b_ - priVars[uIdx]*priVars[uIdx]*priVars[vIdx]);

        return source;
    }

    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
        PrimaryVariables values;

        using std::exp;
        values[uIdx] = a_ + b_
                     + 1.0e-3*exp(-100.0*((globalPos[0] - 1.0/3.0)*(globalPos[0] - 1.0/3.0)
                                        + (globalPos[1] - 0.5)*(globalPos[1] - 0.5)));
        values[vIdx] = b_/((a_ + b_)*(a_ + b_));

        return values;
    }

    Scalar diffCoeff(const Element &element, int eqIdx) const
    {
        const static auto d = getParam<std::array<Scalar, 2>>("Problem.DiffCoeff");

        return d[eqIdx];
    }

private:
    Scalar a_;
    Scalar b_;
    Scalar gamma_;
};

} //end namespace Dumux

#endif // DUMUX_SCHNAKENBERG_PROBLEM_HH
