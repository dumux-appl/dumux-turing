# dune-common
git clone -b releases/2.9 https://gitlab.dune-project.org/core/dune-common.git
git clone -b releases/2.9 https://gitlab.dune-project.org/core/dune-geometry.git
git clone -b releases/2.9 https://gitlab.dune-project.org/core/dune-grid.git
git clone -b releases/2.9 https://gitlab.dune-project.org/core/dune-istl.git
git clone -b releases/2.9 https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone -b releases/2.9 https://gitlab.dune-project.org/staging/dune-uggrid.git
git clone -b releases/2.9 https://gitlab.dune-project.org/extensions/dune-spgrid.git

# dumux
git clone -b releases/3.8 https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
